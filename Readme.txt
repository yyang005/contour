
Parameters list

enable_weighted_gray
1: combine R, G and B channels into one channel
0: not to combine R, G and B channels

fWeight_Red
fWeight_Green
fWeight_Blue
These three parameters control the weight of red, green and blue channel when enable_weighted_gray is enabled. 
When adjust these parameters, make sure the following conditions satisfied. 
0 <= fWeight_Red <= 100
0 <= fWeight_Green <= 100
0 <= fWeight_Blue <= 100
fWeight_Red + fWeight_Green + fWeight_Blue = 100

enable_hsi
1: use H(hue) S(saturation) I(intensity) color space
0: not to use HSI

hue_method
1: extract hue channel by method #1
2: extract hue channel by method #2

choose_channel
1: apply sobel filter on hue channel
2: apply sobel filter on saturation channel
3: apply sobel filter on intensity channel

sobel_mask_size
3: 3x3 mask
5: 5x5 mask

sobel_mask_version
1: version 1
2: version 2

use_sum_gradients
1: use sum of absolute value of gradient in x and y direction
0: not to use sum of absolute value of gradient in x and y direction

use_blackBackground_whiteContour
0: display white contour on black background
1: display black contour on white background
Note: This parameter is useful only when use_sum_gradients is enabled

fWeight_For_3x3_Sobel_Filter
Recommend value: 1.0

fWeight_For_5x5_Sobel_Filter
Recommend value: 1.0, 2.0, 3.0, 5.0, 10.0, 20.0


fFadingFactor
Recommend value: 1.0, 0.7, 0.5
