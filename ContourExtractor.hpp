//
//  ContourExtractor.hpp
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef ContourExtractor_hpp
#define ContourExtractor_hpp

//#include <stdio.h>
#include <vector>
#include "ParametersConfig.h"
#include "image.h"
using namespace imago;

enum HSIComponent {hue = 1, saturation = 2, intensity = 3};
const int grayLevel = 255;
const float feps = 0.000001;
const float pi = 3.14159;
enum ComputHueMehtod {way_1 = 1, way_2 = 2};

const int nIntensityThresholdForSobelMin = 100;
const int nIntensityThresholdForSobelMax = 130;
const vector<vector<float>> vkern3x3_v1{{-1, -2, -1},
                                      {0, 0, 0},
                                      {1, 2, 1}};
const vector<vector<float>> hkern3x3_v1{{-1, 0, 1},
                                      {-2, 0, 2},
                                      {-1, 0, 1}};

const vector<vector<float>> vkern3x3_v2{{-3, -10, -3},
                                       {0, 0,  0},
                                       {3, 10, 3}};
const vector<vector<float>> hkern3x3_v2{{-3, 0, 3},
                                      {-10, 0, 10},
                                       {-3, 0, 3}};

const vector<vector<float>> vkern5x5_v1{{-1, -4, -6, -4, -1},
                                      {-2, -8, -12, -8, -2},
                                      {0, 0, 0, 0, 0},
                                      {2, 8, 12, 8, 2},
                                      {1, 4, 6, 4, 1}};

const vector<vector<float>> hkern5x5_v1{{-1, -2, 0, 2, 1},
                                      {-4, -8, 0, 8, 4},
                                      {-6, -12, 0, 12, 6},
                                      {-4, -8, 0, 8, 4},
                                      {-1, -2, 0, 2, 1}};

const vector<vector<float>> vkern5x5_v2{{-3, -3, -3, -3, -3},
                                      {-10, -10, -10, -10, -10},
                                        {0, 0, 0, 0, 0},
                                        {3, 3, 3, 3, 3},
                                        {10, 10, 10, 10, 10}};

const vector<vector<float>> hkern5x5_v2{{-3, -10, 0, 3, 10},
                                      {-3, -10, 0, 3, 10},
                                      {-3, -10, 0, 3, 10},
                                      {-3, -10, 0, 3, 10},
                                      {-3, -10, 0, 3, 10}};

class Matrix{
public:
    Matrix(){};
    Matrix(int height, int width){
        data = new float[height*width];
    };
    ~Matrix(){
        if (data != nullptr) delete data;
    };
    int width, height;
    float *data=nullptr;
};

class ContourExtractor {
    void rgb2hsi(Image& image, Matrix& outMatrix, HSIComponent component);
    void getGrayWeighted(Image& image, Matrix& grayWeighted);
    void sobelFilter(Matrix& inImage, Matrix& outImage, int& kernelSize, bool useSumGradients, int kernelVersion = 0);
    void maHiFilter(Matrix& inImage, Matrix& outImage, float stdv);
    void generateLOGMask(float stdv);
public:
    ContourExtractor(ParameterConfig& config);
    ~ContourExtractor();
    void contour(Image &image, Image& outPut);
    
    bool _weightedGray = false;
    bool _useHSI = false;
    ComputHueMehtod _hueMethod;
    HSIComponent _hsi;
    bool _use_blackBackground_whiteContour;
    float _fFadingFactor;
    float _fWeightRed;
    float _fWeightGreen;
    float _fWeightBlue;
    float _fWeightFilter;
    
    bool _useSobel;
    int _kernelSize;
    int _kernel3x3Version;
    bool _useSumGradients;
    
    bool _useMahi;
    float _stdvForLogMask;
    vector<vector<float> > logMask;
};

#endif /* ContourExtractor_hpp */
