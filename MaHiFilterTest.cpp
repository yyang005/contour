//
//  MaHiFilterTest.cpp
//  Density
//
//  Created by Anna Yang on 5/14/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include "ContourExtractor.hpp"
#include "ParametersConfig.h"
#include "fileUtil.h"
#include <FreeImage.h>
using namespace std;

int main(int argc, const char * argv[]) {
    if (argc < 3){
        cout << "Please provide path to input file and path to output file!" << endl;
        exit(1);
    }
    
    vector<string> inDirs, outDirs;
    inDirs.push_back(argv[1]);
    outDirs.push_back(argv[2]);
    listdir(argv[1], inDirs, outDirs, argv[2]);
    
    try {
        // Parse the parameter file
        ParameterConfig parser("parameters.txt");
        ContourExtractor extractor(parser);
        for (int i = 0; i < inDirs.size(); i++){
            string inFile(inDirs[i]);
            string outFile(outDirs[i]);
            vector<string> files = getFilePaths(inFile+"/*.jpg");
            vector<string> bmpfiles = getFilePaths(inFile+"/*.bmp");
            vector<string> pngfiles = getFilePaths(inFile+"/*.png");
            files.insert(files.end(), bmpfiles.begin(), bmpfiles.end());
            files.insert(files.end(), pngfiles.begin(), pngfiles.end());
            for (int iFile = 0; iFile < files.size(); iFile++){
                size_t pos = files[iFile].find_last_of("/");
                string imageName = files[iFile].substr(pos+1);
                string file = files[iFile];
                Image image, outImg;
                bool succeed = image.read(files[iFile]);
                if (succeed){
                    extractor.contour(image, outImg);
                    string saveToFile = outFile + "/" + imageName;
                    bool status = outImg.write(saveToFile);
                    if (!status) return -1;
                }
            }
        }
        return 0;
    } catch (string msg) {
        cout << msg << endl;
        exit(1);
    }
}
