//
//  ContourExtractor.cpp
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#include "ContourExtractor.hpp"

ContourExtractor::ContourExtractor(ParameterConfig& config){
    _weightedGray = config.enable_weighted_gray;
    _useHSI = config.enable_hsi;
    
    if (config.choose_channel == 1){
        _hsi = hue;
    }else if (config.choose_channel == 2){
        _hsi = saturation;
    }else if (config.choose_channel == 2){
        _hsi = intensity;
    }
    
    if (config.hue_method == 1){
        _hueMethod = way_1;
    }else if (config.hue_method == 2){
        _hueMethod = way_2;
    }
    
    _fFadingFactor = config.fFadingFactor;
    _fWeightRed = config.fWeight_Red;
    _fWeightGreen = config.fWeight_Green;
    _fWeightBlue = config.fWeight_Blue;
    
    _useSobel = config.use_sobel;
    _useMahi = config.use_ma_hi;
    
    // Sobel kernel
    if (_useSobel){
        _kernelSize = config.sobel_mask_size;
        _kernel3x3Version = config.sobel_mask_version;
        _useSumGradients = config.use_sum_gradients;
        _use_blackBackground_whiteContour = config.use_blackBackground_whiteContour;
        _fWeightFilter = (_kernelSize == 3) ? config.fWeight_For_3x3_Sobel_Filter : config.fWeight_For_5x5_Sobel_Filter;
    }else if (_useMahi){
        // Ma-Hi filter
        generateLOGMask(config.ma_hi_stdv);
    }
};

ContourExtractor::~ContourExtractor(){};

void convolution(Matrix& inImage, Matrix& outMatrix, const vector<vector<float>>& filter){
    int sz = (int)filter.size() / 2;
    int height = inImage.height;
    int width = inImage.width;
    
    if (outMatrix.data == nullptr){
        outMatrix.data = new float[height*width];
        outMatrix.height = height;
        outMatrix.width = width;
    }
    
    for (int j = 0; j < height; j++){
        for (int i = 0; i < width; i++){
            if (j < sz || j > (height-1-sz) || i < sz || i > (width-1-sz)){
                outMatrix.data[j*width+i] = grayLevel;
                continue;
            }
            float sum = 0;
            int startY = j - sz, startX = i - sz;
            for (int jj = 0; jj < filter.size(); jj++){
                for (int ii = 0; ii < filter[0].size(); ii++){
                    int index = (startY + jj) * width + ii + startX;
                    sum += inImage.data[index] * filter[jj][ii];
                }
            }
            outMatrix.data[j*width+i] = sum;
        }
    }
}

void ContourExtractor::sobelFilter(Matrix& inImage, Matrix& outImage, int& kernelSize, bool useSumGradients, int kernelVersion){
    int height = inImage.height;
    int width = inImage.width;
    Matrix vOut, hOut;
    if (kernelVersion == 1){
        if (kernelSize == 3){
            convolution(inImage, vOut, vkern3x3_v1);
            convolution(inImage, hOut, hkern3x3_v1);
        }else if (kernelSize == 5){
            convolution(inImage, vOut, vkern5x5_v1);
            convolution(inImage, hOut, hkern5x5_v1);
        }
    }else if (kernelVersion == 2){
        if (kernelSize == 3){
            convolution(inImage, vOut, vkern3x3_v2);
            convolution(inImage, hOut, hkern3x3_v2);
        }else if (kernelSize == 5){
            convolution(inImage, vOut, vkern5x5_v2);
            convolution(inImage, hOut, hkern5x5_v2);
        }
    }
    
    if (outImage.data == nullptr){
        outImage.data = new float[height*width];
        outImage.height = height;
        outImage.width = width;
    }
    
    if (useSumGradients){
        for (int j = 0; j < vOut.height; j++){
            for (int i = 0; i < vOut.width; i++){
                int index = j*vOut.width+i;
                vOut.data[index] /= _fWeightFilter;
                hOut.data[index] /= _fWeightFilter;
                int sum = abs(vOut.data[index]) + abs(hOut.data[index]);
                if (sum > nIntensityThresholdForSobelMax) sum = 255;
                if (sum < nIntensityThresholdForSobelMin) sum = 0;
                outImage.data[index] = _use_blackBackground_whiteContour ? sum : grayLevel - sum;
            }
        }
    }else{
        for (int j = 0; j < vOut.height; j++){
            for (int i = 0; i < vOut.width; i++){
                int index = j*vOut.width+i;
                vOut.data[index] /= _fWeightFilter;
                hOut.data[index] /= _fWeightFilter;
                int sum = sqrt(vOut.data[index]*vOut.data[index] + hOut.data[index]*hOut.data[index])/4;
                outImage.data[index] = (sum >= nIntensityThresholdForSobelMax)? 0 : 255;
            }
        }
    }
}

void ContourExtractor::maHiFilter(Matrix& inImage, Matrix& outImage, float stdv){
    int height = inImage.height;
    int width = inImage.width;
    Matrix out;
    convolution(inImage, out, logMask);
    
    if (outImage.data == nullptr){
        outImage.data = new float[height*width];
        outImage.height = height;
        outImage.width = width;
    }
    // Zero-crossing
    int contour = 0, background = 255;
    if (_use_blackBackground_whiteContour){
        background = 0;
        contour = 255;
    }
    for (int j = 0; j < out.height; j++){
        for (int i = 0; i < out.width; i++){
            int curIndex = j*out.width+i;
            if (j == 0 || i == 0 || j == height-1 || i == width-1){
                outImage.data[curIndex] = background;
                continue;
            }
            outImage.data[curIndex] = contour;
            if ((out.data[curIndex-1] > 0 && out.data[curIndex+1] < 0) || (out.data[curIndex-1] < 0 && out.data[curIndex+1] > 0)) continue;
            
            if ((out.data[curIndex-out.width] > 0 && out.data[curIndex+out.width] < 0) || (out.data[curIndex-out.width] < 0 && out.data[curIndex+out.width] > 0)) continue;
            
            if ((out.data[curIndex-out.width-1] > 0 && out.data[curIndex+out.width+1] < 0) || (out.data[curIndex-out.width-1] < 0 && out.data[curIndex+out.width+1] > 0)) continue;
            
            if ((out.data[curIndex+out.width-1] > 0 && out.data[curIndex-out.width+1] < 0) || (out.data[curIndex+out.width-1] < 0 && out.data[curIndex-out.width+1] > 0)) continue;
            
            outImage.data[curIndex] = background;
        }
    }
}

void ContourExtractor::generateLOGMask(float stdv){
    int center = (int)3 * stdv;
    _kernelSize = 2 * center + 1;
    logMask.resize(_kernelSize);
    for (int j = 0; j < _kernelSize; j++){
        for (int i = 0; i < _kernelSize; i++){
            float distSquare = (j - center) * (j - center) + (i - center) * (i - center);
            float gauss = exp(-distSquare / (double)(2 * stdv * stdv));
            float res = (distSquare - 2 * stdv * stdv) * gauss / (stdv * stdv * stdv * stdv);
            logMask[j].push_back(res);
        }
    }
}

void matrixToImage(Image& image, Matrix& in, Image& outPut){
    int bytesOfWidth = in.width * sizeof(uchar) * 4;
    if (outPut.width() != in.width || outPut.height() != in.height){
        
        outPut = Image(in.width, in.height, bytesOfWidth);
    }
    for (int j = 0; j < in.height; j++){
        for (int i = 0; i < in.width; i++){
            int index = j*in.width+i;
            outPut.data()[j*bytesOfWidth+i*4] = in.data[index];
            outPut.data()[j*bytesOfWidth+i*4+1] = in.data[index];
            outPut.data()[j*bytesOfWidth+i*4+2] = in.data[index];
            outPut.data()[j*bytesOfWidth+i*4+3] = image(j, i, A);
        }
    }
}

void ContourExtractor::contour(Image &image, Image& outPut){
    Matrix toBeFiltered, filtered;
    if (_useHSI){
        rgb2hsi(image, toBeFiltered, _hsi);
    }else if (_weightedGray){
        getGrayWeighted(image, toBeFiltered);
    }
    
    if (_useSobel){
        sobelFilter(toBeFiltered, filtered, _kernelSize, _useSumGradients, _kernel3x3Version);
    }else if (_useMahi){
        maHiFilter(toBeFiltered, filtered, _stdvForLogMask);
    }
    
    matrixToImage(image, filtered, outPut);
}

void ContourExtractor::rgb2hsi(Image& image, Matrix& outMatrix, HSIComponent component){
    if (outMatrix.data == nullptr){
        outMatrix.data = new float[image.height() * image.width()];
        outMatrix.width = image.width();
        outMatrix.height = image.height();
    }
    float *fHueNotNormalisedArrf = new float[image.height()*image.width()];
    float fHueNotNormalisedMinf = 100, fHueNotNormalisedMaxf = 0;
    for (int j = 0; j < image.height(); j++){
        for (int i = 0; i < image.width(); i++){
            int red = image(j,i,R) * _fFadingFactor;
            int green = image(j,i,G) * _fFadingFactor;
            int blue = image(j,i,B) * _fFadingFactor;
            int gray = (red + green + blue) / 3;
            int index = j * image.width() + i;
            if (component == intensity){
                outMatrix.data[index] = gray;
            }else if (component == saturation){
                unsigned char minColor = min(red, min(green, blue));
                int s;
                // mapping saturation from 0~1 to 0~255
                if (gray != 0){
                    s = (int)((1 - minColor / (float)gray) * grayLevel);
                }else{
                    s = 0;
                }
                outMatrix.data[index] = s;
            }else if (component == hue){
                // Calculate hue
                if (_hueMethod == way_1){
                    float fTemp1f, fTemp2f;
                    fTemp1f = 0.7071 * (green - blue);

                    fTemp2f = 0.816496 * red - 0.40824 * (green + blue);

                    if (fTemp2f < feps && fTemp2f > -feps)
                        fHueNotNormalisedArrf[index] = pi / 2.0;
                    else
                        fHueNotNormalisedArrf[index] = atan2(fTemp1f, fTemp2f);
                }else if (_hueMethod == way_2){
                    float fNumerf, fDenomf, fRatioForHuef;
                    fNumerf = (float)(red - green + red - blue);

                    fDenomf = 2.0*(sqrt((double)((red - green)*(red - green) + (red - blue)*(green - blue))));

                    if (fDenomf < feps && fDenomf > -feps)
                        fHueNotNormalisedArrf[index] = 0.0;
                    else
                    {
                        fRatioForHuef = fNumerf / fDenomf;

                        if (fRatioForHuef < -1.0)
                            fRatioForHuef = -1.0;

                        if (fRatioForHuef > 1.0)
                            fRatioForHuef = 1.0;

                        fHueNotNormalisedArrf[index] = acosf(fRatioForHuef);
                    }
                }
                if (fHueNotNormalisedArrf[index] < fHueNotNormalisedMinf){
                    fHueNotNormalisedMinf = fHueNotNormalisedArrf[index];
                }
                if (fHueNotNormalisedArrf[index] > fHueNotNormalisedMaxf){
                        fHueNotNormalisedMaxf = fHueNotNormalisedArrf[index];
                }
            }
        }
    }

    if (component == hue){
        // Normalized hue and map it to 0~255
        float fHueNotNormaliseRangef = fHueNotNormalisedMaxf - fHueNotNormalisedMinf;
        for (int j = 0; j < image.height(); j++){
            for (int i = 0; i < image.width(); i++){
                int index = j * image.width() + i;
                outMatrix.data[index] = (int)((fHueNotNormalisedArrf[index] - fHueNotNormalisedMinf) / fHueNotNormaliseRangef * grayLevel);
            }
        }
    }
    delete []fHueNotNormalisedArrf;
 }

void ContourExtractor::getGrayWeighted(Image& image, Matrix& grayWeighted){
    int height = image.height();
    int width = image.width();
    if (grayWeighted.data == nullptr){
        grayWeighted.data = new float[height*width];
        grayWeighted.height = height;
        grayWeighted.width = width;
    }
    
    for (int j = 0; j < height; j++) {
        for (int i = 0; i < width; i++) {
            grayWeighted.data[j*width+i] = _fFadingFactor * (image(j,i,R) * _fFadingFactor * _fWeightRed
                                                           + image(j,i,G) * _fFadingFactor * _fWeightGreen
                                                           + image(j,i,B) * _fFadingFactor * _fWeightBlue) / 100;
        }
    }
}

